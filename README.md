# **Algoritmos y Estructura de Datos**
# Guía 0 - Introducción a C++

El programa nos permite ingresar proteínas y sus datos, incluyendo cadenas, aminoácidos, sus átomos y sus coordenadas dentro del plano, además de su posterior visualización. 


## Comenzando
El programa comienza con un menú que da 3 opciones: ver la lista de proteínas que inicialmente está vacía, agregar una proteína y salir del programa.

Al agregar una proteína se solicitan los datos como su nombre y su ID, para luego solicitar cantidad de cadenas y lo que la compone, aminoácidos y cantidad de átomos en esos aminoácidos en conjunto con su localización en el plano. 

Al ver la lista se señala tanto la cantidad de proteínas como cada una con todos sus datos.

En ambos casos se devuelve al menú por si quiere realizar otra acción.

El programa no se detiene hasta seleccionar en el menú la opción 3: salir.

## Prerrequisitos

### Sistema operativo Linux
Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando: 

`sudo apt install make`

Al tener instalado el make solo se debe ejecutar en la terminal el comando `make` dentro de la carpeta con los archivos para crear el programa

**En el caso de tener make o no querer usarlo**

Utilizar el siguiente comando dentro de la carpeta que contiene los archivos del programa:

`g++ programa.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp -o programa`


## Ejecutando las pruebas
Luego de la realización del programa (revisar Prerrequisitos) se debe ejecutar el siguiente comando en la terminal para abrir el programa:

`./programa`

En este se abrirá un menú con las 3 opciones señaladas.

**Opción 1, Ver protreínas:** Simplemente imprime las proteínas en la lista que, si no se agrega ninguna, se mostrará vacía.

**Opción 2, Agregar proteína:** Esta solicitará una serie de datos para poder agregar la proteína. Nombre, ID, Cadena(cantidad entero y letra caracter), Aminoácidos (cantidad entera y nombre caracter), Átomos (cantidad entera, nombre caracter y coordenadas x, y, z racional). Si se ingresan datos del tipo que no corresponde el programa se detendrá automáticamente. Los números también pueden ser parte de los caracteres, es usted, el usuario, quien crea la proteína.

**Opción 3, Salir:** Con esta opción se cerrará el programa.

## Despliegue 
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje C++: librerías iostream, list.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
