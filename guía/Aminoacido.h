#include <iostream>
#include <list>
//#include "Aminoacidos.h"
#include "Atomo.h"
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
        string nombre = "\0";
        int numero = 0;
        list<Atomo> atomos;

    public:
        Aminoacido();
        Aminoacido (string nombre, int numero, list<Atomo> atomos);

        // métodos set y get
        string get_nombre();
        void set_nombre(string nombre);
        int get_numero();
        void set_numero(int numero);
        list<Atomo> get_atomos();
        // método que agrega átomos al aminoácido
        void add_atomos(list<Atomo> atomos);


};
#endif
