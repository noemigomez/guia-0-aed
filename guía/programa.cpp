#include <iostream>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
#include <list>

void imprimir_datos_proteinas(list<Proteina> proteinas){
  cout << "LISTA DE PROTEINAS" << endl;
  cout << "Cantidad de proteínas en la lista: " << proteinas.size() << endl;
  // ciclo que recorre lista de proteínas
  for (Proteina prot: proteinas){
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl; // separador
    cout << "NOMBRE: " << prot.get_nombre() << endl;
    cout << "ID: " << prot.get_id() << endl;
    // ciclo que recorre las cadenas de la proteína
    for(Cadena c: prot.get_cadenas()){
      cout << "Cadena: " << c.get_letra() << endl;
      // ciclo que recorre los aminoácidos de la cadena
      for(Aminoacido a: c.get_aminoacidos()){
        cout << "Aminoácidos" << endl;
        cout << "(" << a.get_numero() << ")" << a.get_nombre() << endl;
        cout << "Los átomos son:" << endl;
        // ciclo que recorre los átomos del aminoácido
        for(Atomo at: a.get_atomos()){
          // coordenada para imprimir
          Coordenada co = at.get_coordenada();
          cout << at.get_numero() << ". "<< at.get_nombre() << endl;
          cout << "x: " << co.get_x() << endl;
          cout << "y: " << co.get_y() << endl;
          cout << "z: " << co.get_z() << endl;
        }
      }
    }
  }
}

list<Proteina> leer_datos_proteinas(list<Proteina> proteinas){
  // definición de variables
  Proteina prot = Proteina();
  string nombre, id;

  cout << "Inserte nombre de proteína: ";
  cin >> nombre;
  prot.set_nombre(nombre);

  cout << "Inserte id: ";
  cin >> id;
  prot.set_id(id);

  list<Cadena> cadena; //cadena vacía para agregar
  prot.add_cadena(cadena);

  //lista con nueva proteína
  proteinas.push_back(prot);
  return proteinas;
}

void menu(list<Proteina> proteinas){
  int opcion;
  cout << "1. Ver proteínas" << endl;
  cout << "2. Agregar proteínas" << endl;
  cout << "3. Salir" << endl;
  cin >> opcion;

  /*Las opciones 1 y 2 retornan al menú por si se quiere realizar algo más */
  if (opcion == 1){
    imprimir_datos_proteinas(proteinas);
    menu(proteinas);
  }
  else if(opcion == 2){
    // retorna nueva lista de proteínas con la agregada
    proteinas = leer_datos_proteinas(proteinas);
    menu(proteinas);
  }
  else if(opcion==3){
    cout << "Muchas gracias! Adiós." << endl;
  }
  else{
    cout << "Opción ingresada no válida. Adiós." << endl;
  }
}

int main(){
  // lista de proteínas inicial vacía
  list<Proteina> proteinas;
  menu(proteinas);
  return 0;
}
