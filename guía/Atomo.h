#include <iostream>
#include <list>
#include "Coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
    private:
        string nombre = "\0";
        int numero = 0;
        Coordenada coordenada;

    public:
        Atomo();
        Atomo (string nombre, int numero, Coordenada coordenadas);

        // metodos set y get
        string get_nombre();
        void set_nombre(string nombre);
        int get_numero();
        void set_numero(int numero);
        Coordenada get_coordenada();
        // método que agrega las coordenadas del átomo
        void add_coordenadas(Coordenada coordenadas);


};
#endif
