#include <iostream>
#include <list>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
    private:
        string letra = "\0";
        list<Aminoacido> aminoacidos;

    public:
        Cadena();
        Cadena (string letra, list<Aminoacido> aminoacidos);

        // metodos set y get
        string get_letra();
        void set_letra(string letra);
        list<Aminoacido> get_aminoacidos();
        void add_aminoacidos(list<Aminoacido> aminoacidos);


};
#endif
