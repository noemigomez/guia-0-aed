#include <iostream>
#include <list>
using namespace std;
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"

//Contructores
Cadena::Cadena(){
  string letra = "\0";
  list<Aminoacido> aminoacidos;
}

Cadena::Cadena(string letra, list<Aminoacido> aminoacidos){
  this->letra = letra;
  this->aminoacidos = aminoacidos;
}

// metodos set y get del objeto
string Cadena::get_letra(){
  return this->letra;
}

void Cadena::set_letra(string letra){
  this->letra = letra;
}

list<Aminoacido> Cadena::get_aminoacidos(){
  return this->aminoacidos;
}

// metodo que agrega aminoácidos a la cadena
void Cadena::add_aminoacidos(list<Aminoacido> aminoacidos){
  int cantidad;
  cout << "Ingrese cantidad de aminoacidos de la cadena " << this->letra << ": ";
  cin >> cantidad;

  for(int i=1; i<cantidad + 1; i++){
    Aminoacido a = Aminoacido();
    string nombre;
    cout << "Ingrese nombre del aminoacido " << i << ": ";
    cin >> nombre;
    a.set_nombre(nombre);
    a.set_numero(i);

    // lista de átomos vacías para agregar
    list<Atomo> atomos;
    a.add_atomos(atomos);

    // agrega aminoácido a lista
    aminoacidos.push_back(a);
  }
  this->aminoacidos = aminoacidos;
}
