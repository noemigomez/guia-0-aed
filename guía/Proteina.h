#include <iostream>
#include <list>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
    private:
        string nombre = "\0";
        string id = "\0";
        list<Cadena> cadena;

    public:
        Proteina();
        Proteina (string nombre, string id, list<Cadena> cadena);

        // metodos set y get
        string get_nombre();
        string get_id();
        list<Cadena> get_cadenas();
        void set_nombre(string nombre);
        void set_id(string id);
        // metodo que agrega la cadena
        void add_cadena(list<Cadena> cadena);


};
#endif
