#include <iostream>
#include <list>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

//Contructores
Aminoacido::Aminoacido(){
  string nombre = "\0";
  int numero = 0;
  list<Atomo> atomos;
}

Aminoacido::Aminoacido(string nombre, int numero, list<Atomo> atomos){
  this->nombre = nombre;
  this->numero = numero;
  this->atomos = atomos;
}

// metodos set y get del objeto
string Aminoacido::get_nombre(){
  return this->nombre;
}

int Aminoacido::get_numero(){
  return this->numero;
}

list<Atomo> Aminoacido::get_atomos(){
  return this->atomos;
}

void Aminoacido::set_nombre(string nombre){
  this->nombre = nombre;
}

void Aminoacido::set_numero(int numero){
  this->numero = numero;
}

// metodo que agrega átomos al aminoácido
void Aminoacido::add_atomos(list<Atomo> atomos){
  int a; // cantidad de átomos
  cout << "Ingrese cantidad de átomos del aminoácido " << this->nombre <<": ";
  cin >> a;
  for(int i=1; i<a+1;i++){
    string at; // nombre del átomo
    Atomo atom = Atomo(); // átomo
    cout << "Ingrese atomo " << i << ":";
    cin >> at;
    atom.set_nombre(at);
    atom.set_numero(i);

    // crea corodenada para agrear
    Coordenada coordenada;
    atom.add_coordenadas(coordenada);

    // agrega átomo
    atomos.push_back(atom);
  }
  this->atomos = atomos;
}
