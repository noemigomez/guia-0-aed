#include <iostream>
#include <list>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"

// Contructores
Proteina::Proteina() {
  string nombre = "\0";
  string id = "\0";
  list<Cadena> cadena;
}

Proteina::Proteina (string nombre, string id, list<Cadena> cadena) {
    this->nombre = nombre;
    this->id = id;
    this->cadena = cadena;
}

// metodos del objeto set y get
string Proteina::get_nombre() {
    return this->nombre;
}

string Proteina::get_id() {
  return this->id;
}

list<Cadena> Proteina::get_cadenas(){
  return this->cadena;
}

void Proteina::set_nombre(string nombre) {
  this->nombre = nombre;
}

void Proteina::set_id(string id) {
  this->id = id;
}

// método que agrega la cadena a la proteína
void Proteina::add_cadena(list<Cadena> cadena){
  int cantidad;
  cout << "Cantidad de cadenas: ";
  cin >> cantidad;
  for(int i=0; i<cantidad; i++){
    string n; // letra que representa la cadena
    Cadena c = Cadena();
    cout << "Ingrese letra de cadena " << i + 1 << ": ";
    cin >> n;
    c.set_letra(n);

    // creación lista de aminoácidos para la cadena
    list<Aminoacido> aminoacidos;
    c.add_aminoacidos(aminoacidos);

    // agregar cadena
    cadena.push_back(c);
    }
  this->cadena = cadena;
}
