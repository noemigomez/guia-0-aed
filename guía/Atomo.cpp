#include <iostream>
#include <list>
using namespace std;
#include "Atomo.h"
#include "Coordenada.h"

//Contructores
Atomo::Atomo(){
  string nombre = "\0";
  int numero = 0;
  Coordenada coordenada;
}

Atomo::Atomo(string nombre, int numero, Coordenada coordenada){
  this->nombre = nombre;
  this->numero = numero;
  this->coordenada = coordenada;
}

// metodos set y get del objeto
string Atomo::get_nombre(){
  return this->nombre;
}

int Atomo::get_numero(){
  return this->numero;
}

Coordenada Atomo::get_coordenada(){
  return this->coordenada;
}

void Atomo::set_nombre(string nombre){
  this->nombre = nombre;
}

void Atomo::set_numero(int numero){
  this->numero = numero;
}

// agrega coordenadas del átomo
void Atomo::add_coordenadas(Coordenada coordenada){
  float x, y, z;
  cout << "Ingrese coordenada x: ";
  cin >> x;
  cout << "Ingrese coordenada y: ";
  cin >> y;
  cout << "Ingrese coordenada z: ";
  cin >> z;
  coordenada.set_x(x);
  coordenada.set_y(y);
  coordenada.set_z(z);
  this->coordenada = coordenada;
}
